@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Xác thực email.') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Đã gửi email xác nhận tới hòm mail của bạn'.') }}
                        </div>
                    @endif

                    {{ __('Để tiếp tục hãy xác nhận email.') }}
                    {{ __('Nếu bạn chưa nhận được email') }}, <a href="{{ route('verification.resend') }}">{{ __('click vào đây để gửi lại email xác thực.') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
