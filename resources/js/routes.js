const routes = [
    {
        path: '/dashboard',
        component: require('./components/Dashboard.vue')
    },
    {
        path: '/profile',
        component: require('./components/Profile.vue')
    },
    {
        path: '/users',
        component: require('./components/Users.vue')
    },
    {
        path: '/products',
        component: require('./components/Products.vue')
    },
    {
        path: '/customers',
        component: require('./components/Customers.vue')
    }
];

export default routes