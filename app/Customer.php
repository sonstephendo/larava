<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    const NEW_CUSTOMER = 'new';
    const TAKING = 'taking';
    const DONE = 'done';

    protected $fillable = ['name', 'email', 'phone', 'product_url', 'user_id', 'status', 'other'];

    public function ref()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
