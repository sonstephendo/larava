<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // provide gate
        $user = auth('api')->user();
        $from = date(Input::get('from'));
        $to = date('Y-m-d', strtotime(Input::get('to') . '+1 days'));
        if (!$to || !$from) {
            $from = date('Y-m-d', strtotime('-30 days'));
            $to = date('Y-m-d', strtotime('1 days'));
        }
        return $user->customers()
            ->whereBetween(Customer::CREATED_AT, [$from, $to])
            ->take(5000)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            // 'name' => 'required|string|max:255',
            // 'email' => 'required|string|email|max:255',
            // 'phone' => 'required|string|max:255',
            // 'product_url' => 'required|string|max:255',
            // 'ref_id' => 'required',
            // 'g-recaptcha-response' => 'required|recaptcha',
        ]);

        $user = User::find($request->ref_id);
        error_log($request);
        if (!$user) {
            $request->ref_id = 1;
        }

        return Customer::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'product_url' => $request->product_url,
            'user_id' => $request->ref_id,
            'status' => Customer::NEW_CUSTOMER,
            'other' => $request->other,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        // $this->authorize('isAdmin');
        $customer->delete();
        return ['message' => 'Customer Deleted'];
    }

    public function update(Customer $customer)
    {
        if ($customer->status == Customer::NEW_CUSTOMER) {
            $customer->status = Customer::TAKING;
        } else if ($customer->status == Customer::TAKING) {
            $customer->status = Customer::DONE;
        } else {
            $customer->status = Customer::NEW_CUSTOMER;
        }
        $customer->save();
        return ['message' => 'Status Updated'];
    }
}
