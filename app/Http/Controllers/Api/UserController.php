<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin');
        return User::orderBy('updated_at', 'desc')->where('id', '!=', 1)->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'photo' => $request->photo,
            'password' => Hash::make($request->password),
        ]);
    }

    public function profile()
    {
        $user = auth('api')->user();
        $user->customers = $user->customersNum();
        $user->products = $user->productsNum();
        return $user;
    }

    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'password' => 'sometimes|string|min:6|confirmed',
        ]);

        // $currentPhoto = $user->photo;

        // if($request->photo != $currentPhoto) {
        //     // get file extension
        //     $imageName = preg_match_all('/data\:image\/([a-zA-Z]+)\;base64/',$request->photo,$matched);
        //     $ext = isset($matched[1]) ? $matched[1] : false;
        //     $imageName = isset($ext[0]) ? time() . '.' .$ext[0] : '' ;
        //     //
        //     $image = \Image::make($request->photo)->save(public_path("img\\profile\\"). $imageName);

        //     $request->merge(['photo' => $imageName]);

        //     $userPhoto = public_path("img\\profile\\"). $currentPhoto;

        //     if (file_exists($userPhoto)) {
        //         @unlink($userPhoto);
        //     }
        // }

        if (!empty($request->password)) {
            $request->merge(['password' => Hash::make($request->password)]);
        }

        $user->update($request->all());
        return ['message' => 'Success'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'password' => 'sometimes|string|min:6|confirmed',
        ]);

        if (!empty($request->password)) {
            $request->merge(['password' => Hash::make($request->password)]);
        }

        $user->update($request->all());

        return ['user' => $user];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('isAdmin');
        $user->delete();
        return ['message' => 'User Deleted'];
    }
}
